package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    // ------------------------------------------------------------------------- assignment element below

    public void run() {

        //round declaration
        System.out.println("Let's play round " + roundCounter);

        while(true)
        {
            // ----------------------------------------------------------- get player move
            
            //get input
            String inp = readInput("Your choice (Rock/Paper/Scissors)?");
            inp = inp.toLowerCase();

            //initialize player choice var
            int plr_choice = -1;

            //get value from string
            for(int i = 0; i < 3; i++)
            {
                if(rpsChoices.get(i).equals(inp))
                {
                    plr_choice = i;
                }
            }

            // ---------------------------------------------------------- compare/confirm moves

            if(plr_choice == -1) //-------------wrong word
            {
                System.out.println("I do not understand " + inp + ". Could you try again?");
            }
            else //-----------------------------correct word
            {
                //AI choice
                Random rand = new Random();
                int cpu_choice = rand.nextInt(3);

                // ----------------------------------------------------- get result
                if(plr_choice == cpu_choice)
                {
                    System.out.println("Human chose " + rpsChoices.get(plr_choice) + ", computer chose " + rpsChoices.get(cpu_choice) + ". Its a tie!");
                }

                if((cpu_choice + 1)%3 == plr_choice)
                {
                    System.out.println("Human chose " + rpsChoices.get(plr_choice) + ", computer chose " + rpsChoices.get(cpu_choice) + ". Human wins!"); 
                    humanScore ++;
                }

                if((plr_choice + 1)%3 == cpu_choice)
                {
                    System.out.println("Human chose " + rpsChoices.get(plr_choice) + ", computer chose " + rpsChoices.get(cpu_choice) + ". Computer wins!"); 
                    computerScore ++;
                }

                //System.out.println(plr_choice + " | " + cpu_choice + " | " + humanScore + " | " + computerScore);

                // ----------------------------------------------------- print score

                System.out.println("Score: human " + humanScore + ", computer " + computerScore);

                // ----------------------------------------------------- new rounds (y/n)
                String awns = readInput("Do you wish to continue playing? (y/n)?");
                if(awns.equals("y"))
                {
                    roundCounter ++;
                    //round declaration
                    System.out.println("Let's play round " + roundCounter);
                }
                else 
                if(awns.equals("n"))
                {
                    break;
                }
                else
                {
                    System.out.println("Uuuuhhmmm, i'll take that as a no");
                    break;
                }
                
            }
        }

        System.out.println("Bye bye :)");

    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
